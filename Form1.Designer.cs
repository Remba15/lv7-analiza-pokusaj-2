﻿namespace Analiza
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonNewGame = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Player1name = new System.Windows.Forms.Label();
            this.Player2name = new System.Windows.Forms.Label();
            this.textBoxPlayer1 = new System.Windows.Forms.TextBox();
            this.textBoxPlayer2 = new System.Windows.Forms.TextBox();
            this.groupBoxLeaderboard = new System.Windows.Forms.GroupBox();
            this.Player1LB = new System.Windows.Forms.Label();
            this.Player2LB = new System.Windows.Forms.Label();
            this.textBoxRez2 = new System.Windows.Forms.TextBox();
            this.textBoxRez1 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBoxLeaderboard.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonNewGame
            // 
            this.buttonNewGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewGame.Location = new System.Drawing.Point(11, 426);
            this.buttonNewGame.Name = "buttonNewGame";
            this.buttonNewGame.Size = new System.Drawing.Size(283, 54);
            this.buttonNewGame.TabIndex = 9;
            this.buttonNewGame.Text = "New Game";
            this.buttonNewGame.UseVisualStyleBackColor = true;
            this.buttonNewGame.Click += new System.EventHandler(this.buttonNewGame_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightGray;
            this.button1.Cursor = System.Windows.Forms.Cursors.Cross;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(6, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 83);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.LightGray;
            this.button2.Cursor = System.Windows.Forms.Cursors.Cross;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(98, 19);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 83);
            this.button2.TabIndex = 11;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.LightGray;
            this.button3.Cursor = System.Windows.Forms.Cursors.Cross;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(190, 19);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(86, 83);
            this.button3.TabIndex = 12;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.LightGray;
            this.button4.Cursor = System.Windows.Forms.Cursors.Cross;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(6, 108);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(86, 83);
            this.button4.TabIndex = 13;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.LightGray;
            this.button5.Cursor = System.Windows.Forms.Cursors.Cross;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(98, 108);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(86, 83);
            this.button5.TabIndex = 14;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.LightGray;
            this.button6.Cursor = System.Windows.Forms.Cursors.Cross;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(190, 108);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(86, 83);
            this.button6.TabIndex = 15;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.LightGray;
            this.button7.Cursor = System.Windows.Forms.Cursors.Cross;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(6, 197);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(86, 83);
            this.button7.TabIndex = 16;
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.LightGray;
            this.button8.Cursor = System.Windows.Forms.Cursors.Cross;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(98, 197);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(86, 83);
            this.button8.TabIndex = 17;
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.LightGray;
            this.button9.Cursor = System.Windows.Forms.Cursors.Cross;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(190, 197);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(86, 83);
            this.button9.TabIndex = 18;
            this.button9.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 124);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(282, 291);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Player1\'s turn";
            // 
            // Player1name
            // 
            this.Player1name.AutoSize = true;
            this.Player1name.Location = new System.Drawing.Point(18, 13);
            this.Player1name.Name = "Player1name";
            this.Player1name.Size = new System.Drawing.Size(93, 13);
            this.Player1name.TabIndex = 20;
            this.Player1name.Text = "Player 1 (X) name:";
            // 
            // Player2name
            // 
            this.Player2name.AutoSize = true;
            this.Player2name.Location = new System.Drawing.Point(18, 37);
            this.Player2name.Name = "Player2name";
            this.Player2name.Size = new System.Drawing.Size(94, 13);
            this.Player2name.TabIndex = 21;
            this.Player2name.Text = "Player 2 (O) name:";
            // 
            // textBoxPlayer1
            // 
            this.textBoxPlayer1.Location = new System.Drawing.Point(110, 8);
            this.textBoxPlayer1.Name = "textBoxPlayer1";
            this.textBoxPlayer1.Size = new System.Drawing.Size(100, 20);
            this.textBoxPlayer1.TabIndex = 22;
            this.textBoxPlayer1.TextChanged += new System.EventHandler(this.textBoxPlayer1_TextChanged);
            // 
            // textBoxPlayer2
            // 
            this.textBoxPlayer2.Location = new System.Drawing.Point(110, 34);
            this.textBoxPlayer2.Name = "textBoxPlayer2";
            this.textBoxPlayer2.Size = new System.Drawing.Size(100, 20);
            this.textBoxPlayer2.TabIndex = 23;
            this.textBoxPlayer2.TextChanged += new System.EventHandler(this.textBoxPlayer2_TextChanged);
            // 
            // groupBoxLeaderboard
            // 
            this.groupBoxLeaderboard.Controls.Add(this.textBoxRez2);
            this.groupBoxLeaderboard.Controls.Add(this.textBoxRez1);
            this.groupBoxLeaderboard.Controls.Add(this.Player2LB);
            this.groupBoxLeaderboard.Controls.Add(this.Player1LB);
            this.groupBoxLeaderboard.Location = new System.Drawing.Point(190, 60);
            this.groupBoxLeaderboard.Name = "groupBoxLeaderboard";
            this.groupBoxLeaderboard.Size = new System.Drawing.Size(104, 64);
            this.groupBoxLeaderboard.TabIndex = 24;
            this.groupBoxLeaderboard.TabStop = false;
            this.groupBoxLeaderboard.Text = "Leaderboard";
            // 
            // Player1LB
            // 
            this.Player1LB.AutoSize = true;
            this.Player1LB.Location = new System.Drawing.Point(6, 16);
            this.Player1LB.Name = "Player1LB";
            this.Player1LB.Size = new System.Drawing.Size(42, 13);
            this.Player1LB.TabIndex = 0;
            this.Player1LB.Text = "Player1";
            // 
            // Player2LB
            // 
            this.Player2LB.AutoSize = true;
            this.Player2LB.Location = new System.Drawing.Point(6, 40);
            this.Player2LB.Name = "Player2LB";
            this.Player2LB.Size = new System.Drawing.Size(42, 13);
            this.Player2LB.TabIndex = 1;
            this.Player2LB.Text = "Player2";
            this.Player2LB.TextChanged += new System.EventHandler(this.Player2LB_TextChanged);
            this.Player2LB.Click += new System.EventHandler(this.Player2LB_Click);
            // 
            // textBoxRez2
            // 
            this.textBoxRez2.Location = new System.Drawing.Point(55, 38);
            this.textBoxRez2.Name = "textBoxRez2";
            this.textBoxRez2.ReadOnly = true;
            this.textBoxRez2.Size = new System.Drawing.Size(40, 20);
            this.textBoxRez2.TabIndex = 3;
            this.textBoxRez2.Text = "0";
            this.textBoxRez2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxRez2.TextChanged += new System.EventHandler(this.textBoxRez2_TextChanged);
            // 
            // textBoxRez1
            // 
            this.textBoxRez1.Location = new System.Drawing.Point(55, 16);
            this.textBoxRez1.Name = "textBoxRez1";
            this.textBoxRez1.ReadOnly = true;
            this.textBoxRez1.Size = new System.Drawing.Size(40, 20);
            this.textBoxRez1.TabIndex = 2;
            this.textBoxRez1.Text = "0";
            this.textBoxRez1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxRez1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 484);
            this.Controls.Add(this.groupBoxLeaderboard);
            this.Controls.Add(this.textBoxPlayer2);
            this.Controls.Add(this.textBoxPlayer1);
            this.Controls.Add(this.Player2name);
            this.Controls.Add(this.Player1name);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonNewGame);
            this.Name = "Form1";
            this.Text = "Tic Tac Toe";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBoxLeaderboard.ResumeLayout(false);
            this.groupBoxLeaderboard.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonNewGame;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Player1name;
        private System.Windows.Forms.Label Player2name;
        private System.Windows.Forms.TextBox textBoxPlayer1;
        private System.Windows.Forms.TextBox textBoxPlayer2;
        private System.Windows.Forms.GroupBox groupBoxLeaderboard;
        private System.Windows.Forms.Label Player2LB;
        private System.Windows.Forms.Label Player1LB;
        private System.Windows.Forms.TextBox textBoxRez2;
        private System.Windows.Forms.TextBox textBoxRez1;
    }
}

