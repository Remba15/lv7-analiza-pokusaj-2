﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
Napravite igru križić-kružić (iks-oks) korištenjem znanja stečenih na ovoj 
laboratorijskoj vježbi. Omogućiti pokretanje igre, unos imena dvaju igrača, ispis
koji igrač je trenutno na potezu, igranje igre s iscrtavanjem križića i kružića na
odgovarajućim mjestima te ispis dijaloga s porukom o pobjedi, odnosno
neriješenom rezultatu kao i praćenje ukupnog rezultata.  
*/

namespace Analiza
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MessageBox.Show("Welcome to Tic Tac Toe Game!","Welcome");
            MessageBox.Show("Please enter player names before you start.", "Notice");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach(Control c in groupBox1.Controls)
            {
                if(c is Button)
                {
                    c.Click += new System.EventHandler(btn_click);
                }
            }
        }

        

        int XorO = 0;
        public void btn_click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Text == "")
            {
                
                if (XorO % 2 == 0)
                {
                    btn.Text = "X";
                    btn.ForeColor = Color.Red;
                    groupBox1.Text = Player2LB.Text + "'s turn";
                    getWinner();
                }
                else
                {
                    btn.Text = "O";
                    btn.ForeColor = Color.Blue;
                    groupBox1.Text = Player1LB.Text + "'s turn";
                    getWinner();
                }

                XorO++;
            }

            
        }

        int winXcount = 0;
        int winOcount = 0;
        bool win = false;
        public void getWinner()
        {
            if(!button1.Text.Equals("") && button1.Text.Equals(button2.Text) && button1.Text.Equals(button3.Text))
            {
                if (button1.Text == "X")
                {
                    winXcount++;
                }
                if(button1.Text == "O")
                {
                    winOcount++;
                }
                winHighlight(button1, button2, button3);
                win = true;
            }
            if (!button4.Text.Equals("") && button4.Text.Equals(button5.Text) && button4.Text.Equals(button6.Text))
            {
                if (button4.Text == "X")
                {
                    winXcount++;
                }
                if (button4.Text == "O")
                {
                    winOcount++;
                }
                winHighlight(button4, button5, button6);
                win = true;
            }
            if (!button7.Text.Equals("") && button7.Text.Equals(button8.Text) && button7.Text.Equals(button9.Text))
            {
                if (button7.Text == "X")
                {
                    winXcount++;
                }
                if (button7.Text == "O")
                {
                    winOcount++;
                }
                winHighlight(button7, button8, button9);
                win = true;
            }
            if (!button1.Text.Equals("") && button1.Text.Equals(button4.Text) && button1.Text.Equals(button7.Text))
            {
                if (button1.Text == "X")
                {
                    winXcount++;
                }
                if (button1.Text == "O")
                {
                    winOcount++;
                }
                winHighlight(button1, button4, button7);
                win = true;
            }
            if (!button2.Text.Equals("") && button2.Text.Equals(button5.Text) && button2.Text.Equals(button8.Text))
            {
                if (button2.Text == "X")
                {
                    winXcount++;
                }
                if (button2.Text == "O")
                {
                    winOcount++;
                }
                winHighlight(button2, button5, button8);
                win = true;
            }
            if (!button3.Text.Equals("") && button3.Text.Equals(button6.Text) && button3.Text.Equals(button9.Text))
            {
                if (button3.Text == "X")
                {
                    winXcount++;
                }
                if (button3.Text == "O")
                {
                    winOcount++;
                }
                winHighlight(button3, button6, button9);
                win = true;
            }
            if (!button1.Text.Equals("") && button1.Text.Equals(button5.Text) && button1.Text.Equals(button9.Text))
            {
                if (button1.Text == "X")
                {
                    winXcount++;
                }
                if (button1.Text == "O")
                {
                    winOcount++;
                }
                winHighlight(button1, button5, button9);
                win = true;
            }
            if (!button3.Text.Equals("") && button3.Text.Equals(button5.Text) && button3.Text.Equals(button7.Text))
            {
                if (button3.Text == "X")
                {
                    winXcount++;
                }
                if (button3.Text == "O")
                {
                    winOcount++;
                }
                winHighlight(button3, button5, button7);
                win = true;
            }

            if (fullButtons() == 9 && win == false)
            {
                groupBox1.Text = "NO WINNER";
            }
            
        }

        

        public int fullButtons()
        {
            int textButtonLength = 0;
            foreach (Control c in groupBox1.Controls)
            {
                if (c is Button)
                {
                    textButtonLength += c.Text.Length;
                }
            }
            return textButtonLength;
        }

        public void winHighlight(Button b1, Button b2, Button b3)
        {
            b1.BackColor = Color.Green;
            b2.BackColor = Color.Green;
            b3.BackColor = Color.Green;

            groupBox1.Text = "WINNER!!";
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            button5.Enabled = false;
            button6.Enabled = false;
            button7.Enabled = false;
            button8.Enabled = false;
            button9.Enabled = false;

            textBoxRez1.Text = winXcount.ToString();
            textBoxRez2.Text = winOcount.ToString();
        }

        private void buttonNewGame_Click(object sender, EventArgs e)
        {
            XorO = 0;
            win = false;
            foreach (Control c in groupBox1.Controls)
            {
                if (c is Button)
                {
                    c.Text = "";
                    c.BackColor = Color.LightGray;
                    groupBox1.Text = Player1LB.Text + "'s turn";
                }
            }
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = true;
            button8.Enabled = true;
            button9.Enabled = true;
        }

        

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBoxRez2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBoxPlayer1_TextChanged(object sender, EventArgs e)
        {
            Player1LB.Text = textBoxPlayer1.Text;
        }

        private void Player2LB_Click(object sender, EventArgs e)
        {
            
        }

        private void Player2LB_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBoxPlayer2_TextChanged(object sender, EventArgs e)
        {
            Player2LB.Text = textBoxPlayer2.Text;
        }
    }
}
